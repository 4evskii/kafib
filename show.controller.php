<?php
echo '
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>';
header('Content-Type: text/html; charset=utf-8');
define('ISMPORTAL', TRUE);
$a=new siteShow;
$a->index ();

class Point { //точка на плоскости
	public $x;
	public $y;

	public function __construct($x, $y) {
		 $this -> x = $x;
		 $this -> y = $y;
	}
}

class Rect { //прямоугольник на плоскости
	public $x1;
	public $y1;
	public $x2;
	public $y2;
	
	public function __construct($x1, $y1, $x2, $y2) {
		$this -> x1 = $x1;
		$this -> y1 = $y1;
		$this -> x2 = $x2;
		$this -> y2 = $y2;
	}
}

class Vector { //двумерный вектор
	public $x;
	public $y;
	
	public function __construct($x, $y) {
		$this -> x = $x;
		$this -> y = $y;
	}
	
	public function Length() {
		return sqrt($x * $x + $y * $y);
	}
	
	public function Multiply($c) {
		$x *= $c;
		$y *= $c;
	}
	
	public function Dot($v) { //векторное произведение, с помощью него удобно находить пересечение двух отрезков на плоскости
		return $this -> x * $v -> y - $this -> y * $v -> x;
	}		
}

class SegmentRectangleIntersection { //Пересечение отрезка, соединяющего центры двух прямоугольников, с границами прямоугольников
	public $pt1; //центр первого прямоугольника
	public $pt2; //центр второго прямоугольника
	//параметры второго прямоугольника
	public $Top; 
	public $Left;
	public $Width;
	public $Height;
	
	public function __construct($x1, $y1, $x2, $y2, $Top, $Left, $Width, $Height) {
		$this -> pt1 = new Point($x1, $y1);
		$this -> pt2 = new Point($x2, $y2);
		$this -> Top = $Top;
		$this -> Left = $Left;
		$this -> Width = $Width;
		$this -> Height = $Height;			
	}
		
	public function Intersection() {

		//последовательно рассматриваем пересечения отрезка, соединяющего точки pt1 и pt2, с границами второго прямоугольника (всего 4 варианта) 
		$r = new Vector($this -> pt2 -> x - $this -> pt1 -> x, $this -> pt2 -> y - $this -> pt1 -> y);
		$p = new Point($this -> pt1 -> x, $this -> pt1 -> y);
				
		$q = new Point($this -> Left, $this -> Top);
		$s = new Vector(0, $this -> Height);
		$rxs = $r -> Dot($s);			
		$delta = new Vector($q -> x - $p -> x, $q -> y - $p -> y);		
		
		if (abs($rxs) > 0.0001) {
			$t = $delta -> Dot($s) / $rxs;
			$u = $delta -> Dot($r) / $rxs;

			if (($t >= 0) and ($t <= 1) and ($u >= 0) and ($u <= 1)) {
				return new Point($p -> x + $r -> x * $t, $p -> y + $r -> y * $t);			
			}
		}
		
		$q = new Point($this -> Left, $this -> Top);
		$s = new Vector($this -> Width, 0);
		$rxs = $r -> Dot($s);			
		$delta = new Vector($q -> x - $p -> x, $q -> y - $p -> y);	

		if (abs($rxs) > 0.0001) {
			$t = $delta -> Dot($s) / $rxs;
			$u = $delta -> Dot($r) / $rxs;

			if (($t >= 0) and ($t <= 1) and ($u >= 0) and ($u <= 1)) {
				return new Point($p -> x + $r -> x * $t, $p -> y + $r -> y * $t);			
			}
		}	
		
		
		$q = new Point($this -> Left, $this -> Top + $this -> Height);
		$s = new Vector($this -> Width, 0);
		$rxs = $r -> Dot($s);			
		$delta = new Vector($q -> x - $p -> x, $q -> y - $p -> y);	
		
		if (abs($rxs) > 0.0001) {
			$t = $delta -> Dot($s) / $rxs;
			$u = $delta -> Dot($r) / $rxs;

			if (($t >= 0) and ($t <= 1) and ($u >= 0) and ($u <= 1)) {
				return new Point($p -> x + $r -> x * $t, $p -> y + $r -> y * $t);			
			}
		}
				
		$q = new Point($this -> Left + $this -> Width, $this -> Top);
		$s = new Vector(0, $this -> Height);
		$rxs = $r -> Dot($s);			
		$delta = new Vector($q -> x - $p -> x, $q -> y - $p -> y);
		
		if (abs($rxs) > 0.0001) {
			$t = $delta -> Dot($s) / $rxs;
			$u = $delta -> Dot($r) / $rxs;

			if (($t >= 0) and ($t <= 1) and ($u >= 0) and ($u <= 1)) {
				return new Point($p -> x + $r -> x * $t, $p -> y + $r -> y * $t);			
			}
		}		

		return new Point(0, 0); // не должна доходить сюда никогда	
			
	}
}


class drawText {
		public $newpt;
		public $txt;
		public function __construct($x3, $y3,$caption) {
		$this -> newpt = new Point($x3, $y3);
		$this -> txt = new Text($caption);
		}
		public function DrawText () { 
		echo '					
		<canvas id="drawtext" width=762px; height=626px; ></canvas>
		<script>
		function wrapText(context, text, Left, Top, maxWidth, lineHeight,mText)
						{
					var halfmText=(mText/2);
					var trueLeft=Left-halfmText;
					var trueTop=Top+15;
					context.fillText(text, trueLeft,trueTop);
					}
					
					
					
					
					
					
					
					
					
					var canvas=document.getElementById("drawtext");
					var context=canvas.getContext("2d");
					var maxWidth = 42; //размер поле, где выводится текст
					var lineHeight = 16;
					var Left =((' . $this -> newpt -> x . ')+18); //
					var Top =((' . $this -> newpt -> y . ')+32);
					var text = "' . $this -> txt .'" ;
					context.font = "normal 12px Tahoma";
					context.fillStyle = "#000";
					var mText=context.measureText(text).width;
					wrapText(context, text, Left, Top, maxWidth, lineHeight,mText);
					
					
					
					
		</script>


';

		}		

}

class ConnectMapObjects { //соединяет объекты стрелками
	public $pt1;
	public $pt2;
     //координаты для текста
	public $text;
	public function __construct($x1, $y1, $x2, $y2) {
		$this -> pt1 = new Point($x1, $y1);
		$this -> pt2 = new Point($x2, $y2);
		
	}	
		
	public function DrawLine($color) { //рисует отрезок между pt1 и pt2
		echo '
				<canvas id="links" width=762px; height=626px; ></canvas>
				<script>
					var canvas=document.getElementById("links");
					var context=canvas.getContext("2d");
					context.beginPath ();
					context.moveTo('. number_format($this -> pt1 -> x) . ',' . number_format($this -> pt1 -> y) . ');
					context.lineTo('. number_format($this -> pt2 -> x) . ',' . number_format($this -> pt2 -> y) . ');
					context.lineWidth = 2;
					context.strokeStyle = "' . $color . '"	;
					context.stroke();
					
						
					
					
					
					
					
				</script>';

	}	

	public function Connect($color) {		
		$inter = new SegmentRectangleIntersection($this -> pt1 -> x, $this -> pt1 -> y, $this -> pt2 -> x, $this -> pt2 -> y, $this -> pt2 -> y - 16, $this -> pt2 -> x - 18, 36, 32);
				
		$this -> pt2 = $inter -> Intersection();
		$this -> DrawLine($color);
		
		$line_vector = new Vector($this -> pt2 -> x - $this -> pt1 -> x, $this -> pt2 -> y - $this -> pt1 -> y);		
		
		//рисуем стрелки
		$arrow_length = 10;
		for ($i = -1; $i <= 1; $i += 2) {
			$phi = $i * pi() / 6;
				
			$x = cos($phi) * $line_vector -> x + sin($phi) * $line_vector -> y;
			
			$y = - sin($phi) * $line_vector -> x + cos($phi) * $line_vector -> y;
			$x = $arrow_length * $x / sqrt($x * $x + $y * $y);
			$y = $arrow_length * $y / sqrt($x * $x + $y * $y);
		
			$this -> pt1 -> x = $this -> pt2 -> x - $x;
			$this -> pt1 -> y = $this -> pt2 -> y - $y;			
			$this -> DrawLine($color);		
		}			
	}

	
}

class Connection { //связь
	public $id_to;
	public $sign;
	
	public function __construct($id_to, $sign) {
		$this -> id_to = $id_to;
		$this -> sign = $sign;
	}	
}
class Text {
	public $caption;
	
	public function __construct ($caption){
		$this -> caption = $caption;
	}
	
	public function __toString(){
	return $this -> caption;
	} 
	
}


class siteShow { 
	public function index () {
		
		$map_id = $_POST['name'];
		echo '<div id="formap7">';
		echo'
		<script type="text/javascript">
		function str_ends_with(str, prefix_current) {
		return str.indexOf(prefix_current, str.length - prefix_current.length) !== -1;						
		}

		function mynamebutton(){
							
							
							$("#formap7 img").each(function(){
								var id=$(this).attr("id");
							var str = $("#" + id).attr("src");
						
							var prefix_current="_current.jpg";
							if (str_ends_with(str, prefix_current)) {
								str = str.substr(0, str.length - prefix_current.length);
								var prefix_jpeg=".jpg"
								$("#" + id).each( function () {this.src=str+prefix_jpeg; } );
								}
								 });
						}

		function str_ends_with(str, prefix_selected) {
		return str.indexOf(prefix_selected, str.length - prefix_selected.length) !== -1;							
		}
						
						

		function currentimage(id) {
								mynamebutton();
								var prefix_current="_selected_current.jpg";
								var str=$("#" + id).attr("src");							
								var prefix_selected="_selected.jpg";
								
								if (str_ends_with(str, prefix_selected)) {
								str = str.substr(0, str.length - prefix_selected.length);
								var newsrc = str + prefix_current;
								$("#" + id).each( function () {this.src=newsrc; } );
								} else {
								str = str.substr(0,str.length-4);
								var newsrc = str + prefix_current;
								$("#" + id).each( function () {this.src=newsrc; } );
								} 

								
								
						   }
		function func(id) {
			
		
										

			var type = $("#" + id).attr("type");
			if (type != 9) {
			var zero_id = $("#" + id).attr("zero_id");
			alert("zero_id = " + zero_id + "\n" + "id = " + id);
			} else {
			var positive_id = $("#" + id).attr("positive_id");
			var negative_id = $("#" + id).attr("negative_id");
			alert("positive_id = " + positive_id + " , negative_id = " + negative_id);
		
			}
	
		// alert($("#" + id).attr("caption"));
			}



		</script>';		 

		
		$dbName = ' (DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.0.18)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=ORCL))) ';
		$dbUser = ' EAD_KSO_DATA ';
		$dbPass = ' EAD_KSO_DATA ';
		$dbCharset = ' CL8MSWIN1251 ';
		$con = oci_connect($dbUser, $dbPass,$dbName); 		
		  
		$query = "SELECT application_objects.id, x, y, type, caption, object_name, id_form, id_to, sign FROM application_objects LEFT JOIN application_links ON (application_links.id_from=application_objects.id) WHERE application_map_id=$map_id";
		$result = oci_parse($con, $query); 		
		oci_execute($result,OCI_DEFAULT);
		
		$img_srcs = array();
		$points = array();
		$connections = array();
		$captions = array ();
		
		while($rowNike = oci_fetch_assoc($result)){
			$x = $rowNike['X'];
			$y = $rowNike['Y'];
			$id = $rowNike['ID'];
			$type = $rowNike['TYPE'];
			$caption =  iconv('windows-1251', 'utf-8',$rowNike['CAPTION']);
			$object_name = $rowNike['OBJECT_NAME'];
			$id_form = $rowNike['ID_FORM'];
			$sign = $rowNike['SIGN'];
			$id_to = $rowNike['ID_TO'];
			
			if (!array_key_exists($id, $img_srcs)) {
				switch ($type){
					case 1:
						$img = "download_template.jpg";
						$img1 = "download_template_selected.jpg";
						$img2 = "download_template_selected_current.jpg";
						break;
					case 2:
						$img = "data_template.jpg";	
						$img1 = "data_template_selected.jpg";
						$img2 = "data_template_selected_current.jpg";
						break;
					case 3:
						$img = "download.jpg";		
						$img1 = "download_selected.jpg";		
						$img2 = "download_selected.jpg_current";	
						break;
					case 4:
						$img = "constraint.jpg";
						$img1 = "constraint_selected.jpg";
						$img2 = "constraint_selected_current.jpg";
						break;
					case 5:
						$img = "arrow.jpg";
						$img1 = "arrow.jpg_selected";
						$img2 = "arrow_selected_current.jpg";
						break;
					case 6: 
						$img = "protocols.jpg";
						$img1 = "protocols_selected.jpg";
						$img2 = "protocols_selected_current.jpg";
						break;
					case 7:
						$img = "diagram.jpg";	
                        $img1 = "diagram_selected.jpg";							
						$img2 = "diagram_selected_current.jpg";							
						break;
					case 8:
						$img = "report.jpg";
						$img1 = "report_selected.jpg";
						$img2 = "report_selected_current.jpg";
						break;
					case 9:
						$img = "circle.jpg";
						$img1 = "circle_selected.jpg";
						$img2 = "circle_selected_current.jpg";
						break;
				
				}
				
				
				$point = new Point($x, $y);
				$points[$id] = $point;
				
				$connection = array();
				if ($id_to != null) {
					$connection[0] = new Connection($id_to, $sign);
				}
				$connections[$id] = $connection;
					
				$cap = new Text($caption);
				$captions[$id] = $cap;
				
				




			
				$img_srcs[$id] = '<img src="' . $img . '" src_selected="' . $img1 . '" src_current="' . $img2. '" type="' . $type . '"  class = "click" id="'. $id .'"  onclick="currentimage(' . $id . ');func(' . $id . ')" alt="" style="position:absolute;top:'. $y .'px ; left:'. $x .'px" ';
			} else {
				$connection = $connections[$id];
				if ($id_to != null) {
					$connection[1] = new Connection($id_to, $sign);
				}
				$connections[$id] = $connection;
			}			
			
			if ($sign == 1) {
				$img_srcs[$id] .= ' positive_id="' . $id_to . '"';
			} else if ($sign == -1) {
				$img_srcs[$id] .= ' negative_id="' . $id_to . '"';
			} else if ($sign == 0) {
				$img_srcs[$id] .= ' zero_id="' . $id_to . '"'; 
			} else {
				die("Неизвестынй sign = $sign");
			}			
		}
		foreach ($img_srcs as $key => $value) {
		//	echo $value . ">";

		draw_arrow($key, $con, $points, $connections, $captions);
		}
		foreach ($img_srcs as $key => $value) {
			echo $value . ">";

	//	draw_arrow($key, $con, $points, $connections);
		}
		
		$z=count($img_srcs);
		reset($img_srcs);
		$k=key($img_srcs);
		$kz=$k+$z;
		echo '</div>';
		echo '
		<script type="text/javascript">
		function showallatrib(k){
		
		var kz=' . $kz . ';
		
				for (k;k<kz;k++)
				{
					
					$("#" + k).each( function(){ this.src = this.getAttribute("src_current"); } );
					var type = $("#" + k).attr("type");
					if (type !=9){
					var id = $("#" + k).attr("id"); 
					var caption = $("#" + k).attr("caption"); 
					var zero_id = $("#" + k).attr("zero_id");
					if (zero_id==0){
					throw "Операция завершена";} else {
					alert("id="+id+"\n"+"type="+type+"\n"+"caption="+caption+"\n"+"zero_id=" + zero_id);
					showallatrib(zero_id);
					
					}
					alert("id="+id+"\n"+"type="+type+"\n"+"caption="+caption+"\n"+"zero_id=" + zero_id);
					} else {
					var id = $("#" + k).attr("id"); 
					var caption = $("#" + k).attr("caption");
					var positive_id = $("#" + k).attr("positive_id");
					var negative_id = $("#" + k).attr("negative_id");
					alert("id="+id+"\n"+"type="+type+"\n"+"caption="+caption+"\n"+"positive_id=" + positive_id+"\n"+"negative_id="+negative_id);
					function selectnextelem () {
					var a=Math.random();
					var b=Math.random();
					if (a<b){
					k=positive_id;
					} else {
					k=negative_id;}
					
					alert ("NextID="+k);
					showallatrib(k);
					}
					selectnextelem ();
					}
					
				}
				
		}
		
		
		</script>';
		echo '<button id="mapbutton" onclick="showallatrib(' . $k . ');" >seeallelem</button>';
		
		echo '</div>';
	}
	
	
}		
								
function draw_arrow($id, $con, $points, $connections , $captions)
{

	$point = $points[$id];
	$connection = $connections[$id];
	$cap = $captions[$id];
	$caption = $cap -> caption;
	
	
	$x1 = $point -> x; //$x3 это x1-5
	$y1 = $point -> y; //$y3 это y1+32

	
	$x3 = $x1 ;
	$y3 = $y1 ;
	
	$drawText= new DrawText($x3,$y3,$caption);
	$drawText -> DrawText();
	foreach ($connection as $value) {
		$id_to = $value -> id_to;
		$sign = $value -> sign;
		
		switch ($sign) {
			case 0:
				$color = "blue";
				break;
			case 1:
				$color = "green";
				break;
			case -1:
				$color = "red";
				break;
		}
		
		$point = $points[$id_to];	
		
		$x2 = $point -> x;
		$y2 = $point -> y;
		
			
		//$cap = $captions[$id_to];
		//$caption1 = $cap -> caption;
        //var_dump($caption1);	
		$connect_map_object = new ConnectMapObjects($x1 + 18, $y1 + 16, $x2 + 18, $y2 + 16);
		
		$connect_map_object -> Connect($color);		
	}	
}


?>		

